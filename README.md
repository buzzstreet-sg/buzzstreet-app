# buzzstreet-app

BuzzStreet revolutionizes your daily commute with real-time traffic and weather insights. 
Choose a date and time to view traffic cam images and weather forecasts at specific locations. 
It simplifies your journey with user-friendly location names, recent search suggestions, 
and comprehensive search reports.

## Technical Solutions

### Assumptions Made

- This is a medium size application.
  - Target to be serving around 2000-5000 people over the day.
  - Target to be serving around 100-200 concurrent public users at any one time.


- The application is envisioned to help users check weather and traffic conditions so that they can plan their trip around Singapore.
  - Users are mostly concern with the present (current) data, and do not need data that are old.
  - The application only need to show the last 7 days of traffic and weather data.
  - User querying traffic and weather data past 7 days will not get any data displayed.


- Due to the use case of the application stated above, absolute real-time data is not required, it is acceptable for near real-time data to be displayed.
  - Up to 5-10 delay in traffic data. (So seeing 10:01 AM data when querying 10:05 AM data is acceptable.)
  - Up to 20-30 delay in traffic data. (So seeing 10:01 AM data when querying 10:20 AM data is acceptable.)


- The application is not envision to become a data warehouse for the weather and traffic data.
  - Weather and traffic data past the supported number of days can be cleared.
  - However, users' search transaction history records will continue to be stored.


- The application will adopt data archiving strategy to ensure performance does not degrade over time as the data size increases.
  - Queryable data stored by the application will be up to 5 year.
  - There will be maintenance every year during the wee hours of off-peak season and to archive data more than 5 years.


- The application will adopt rate-limiting strategy on heavy computation API services.
  - This will include the report API to query top searches and busiest period.


- Location of the traffic and weather data does not change frequently.
  - Lat-long coordinates to location name mapping can be cached and used for 99% of the time.


- Traffic camera image(s) storage is host on different infrastructure as the data API service(s), and it is highly available.
  - So it does not require caching to cater for downtime scenarios.


### High-Level Solution / Strategy

1. Applications will be deployed on AWS.


2. Front-end single-page application (SPA). 
   - It is in-charge of client side display and user interactions.
   - It is compiled and stored in S3, and served with AWS CloudFront.
   - It is using ReactJS with TypeScript.


3. Back-end server-side app.
   - It is in-charge of hosting API services and connection to Redis and PostgreSQL.
   - It is compiled and containerized and stored in AWS Elastic Container Registry (ECR).
   - It is served on AWS Fargate and managed by AWS Elastic Container Service (ECS).
   - It is using NestJS, TypeScript and TypeORM.


4. PostgreSQL database.
   - It is used to store user search history records.
   - It is hosted with Amazon Relational Database Service (RDS).


5. Redis.
   - It is used to store location, traffic and weather data.
   - It is hosted on Amazon Elastic Cloud Compute (EC2).
   - It is configured to write to disk, therefore data will not be lost there is any crashes / downtime.
   - Able to set cache expiry: allows automatic clearing of data


6. Scheduled / CRON jobs.
   - 3 scripts to be run at fixed intervals:
      - Geocoding: To fetch the locations in traffic and weather data, reverse the lat and long coordinates to human-understandable location names.
        - Runs every 1 hour (configurable). Data processed stored in Redis.
      - Traffic: To fetch traffic data, convert lat-long coordinates to human-understandable location names.
        - Runs every 5-10 minutes (configurable). Data processed stored in Redis.
      - Weather: To fetch weather data.
        - Runs every 20-30 minutes (configurable). Data processed stored in Redis
   - These scripts will be compiled and stored in AWS S3.
   - Using AWS Cloudwatch Event to trigger at the defined frequency, and AWS Lambda to run the scripts.


7. API Gateway will be used for rate-limiting for heavy reports API (top searches, busiest period).


### Key Considerations & Decisions

1. Since the application is medium-sized with significant number of concurrent users, 
   the application cannot afford to poll results on demand from rate-limited external API services.
   - Therefore, we have to consider a CRON job that does the polling on an interval.
   - NestJS provides a scheduler feature, but if we are running multiple instances, the scheduler
     will not be aware of each other and will fetch the data more times than required.
   - Rather than setting up another NestJS instance or do some special handling, decision is to 
     make things simpler with 3 basic scripts and relying on AWS Cloudwatch Event and Lambda.


2. Storing of traffic data and weather data:
   - If application starts to store these data indefinitely, the data size can pile up very fast.
   - Also, we will have to start to worry about syncing of data from the source.
   - This will make the application maintenance more complex than necessary.
   - Moreover, older traffic and weather forecast results does not seem to serve any use case defined.
   - Therefore, assumption/decision is that application is not going to be a data warehouse/storage 
     for traffic and weather, data will be cleared after a configurable duration.


3. Traffic data and weather data can be stored/cached in either the PostgreSQL or the Redis instance.
   - Since we will be clearing the data, consideration will be the size of data and ease of clearing the data.
   - PostgreSQL can store more data as it uses disk space, Redis less so since it uses RAM. Since 
     data size is relatively small (assumed 7 days worth), both solutions are viable.
   - Redis allows data to have an expiry date / time-to-live duration, which makes it very 
     straightforward to clear data, as compared to PostgreSQL.
   - Redis is generally faster than PostgreSQL in fetching records, allowing better user 
     experience with lower waiting time.
   - Due to the nature of the data being not real-time (no delay) but near real-time (got some delay), 
     and the assumption that a delayed data is acceptable, we can slow down the polling of data and
     reduce the size of the data further.
   - Therefore, the decision is to use Redis to store / cache the data.


4. Report APIs to support is computationally heavy.
   - It is generally not a good idea to fetch large amount of data and process it on the API server. 
     This will potentially pose high RAM and CPU usage to the app server(s).
   - So the decision is to run the heavy computation in the database.
   - On top of that, we have to index the record created timestamp column to allow faster computation.
   - On top of that, we will have to put rate-limit on the API to safeguard the performance of 
     the database which will affect the application and user experience.
   - If required, we can spin up multiple read-replicas of the database and point the report APIs to the read-replicas.
   - Also recommends data archiving of the database to ensure the data size is healthy for the 
     application to function with consistent performance.


## Location Development Setup

This section will guide you through setting up your local development environment.

Before you start, please be aware that the application project is using git submodule.
- buzzstreet-app is the main parent repository,
- buzzstreet-backend-app is a git submodule containing the backend app repository.
- buzzstreet-frontend-app is a git submodule containing the fronten SPA repository.

For more information on git submodule, refer to https://git-scm.com/book/en/v2/Git-Tools-Submodules

The steps will assume that you already have cloned the repository.

### 1. Install Node.js

Node.js is a JavaScript runtime environment that allows us to execute JavaScript code outside a web browser.

Download and install the latest version of Node.js at: https://nodejs.org/en/


### 2. Download and Install VirtualBox (for Windows) and Parallels Pro (for Mac)

VirtualBox and Parallels allow us to create virtual machine (VM) to run our applications.

Download VirtualBox: https://www.virtualbox.org/wiki/Downloads

Download Parallels Pro: https://www.parallels.com/products/desktop/buy/?full



### 3. Download and Install Vagrant

Vagrant is a VM manager, it will help to install and provision virtual machines seamlessly.

Download and install Vagrant at https://developer.hashicorp.com/vagrant/install


### 4. Create and Provision the VM using Vagrant Commands

Open up this repository with your choice of code editor.

Alternatively, you can use a command line interface such as command prompt, git bash, terminal.

Go to the root directory of the backend app repository.

Run the following vagrant command to create and provision your VM:

```shell
vagrant up
```

Based on the `Vagrantfile` at the root of the repository, vagrant will create the VM running on Amazon Linux 2.

After which, `docker` and `docker-compose` will be installed into the environment.

`docker` and `docker-compose` will allow us to host the PostgreSQL and Redis instances. It also allows us to trigger the CRON jobs to run, simulating the AWS Cloudwatch Event and AWS lambda running the scripts to keep our Redis instance populated with data.

Before we can run the resources, we will have compile and ready of our scripts first. Refer to the next section on how to do that.


### 5. Compiling the Scripts on the `buzzstreet-backend-app` Repository.

Open up the backend-app repository with your choice of code editor.

Once in the repository, ensure you have the `.env` file, and make sure the following configuration are available.

For the full environment variable to be added, refer to the README in the backend app repository, for the context of the app setup, we will just focus on the following configurations:

```.dotenv

... other env ...

## Redis
REDIS_HOST=172.31.22.155
REDIS_PORT=6379
REDIS_TRAFFIC_TTL_SEC=3600
REDIS_WEATHER_TTL_SEC=3600

... other env ...

```

Once done, we will build and compile the app.

You can use the terminal in the IDE to do so, or alternatively, you can use a command line interface such as command prompt, git bash, terminal.

Go to the root directory of the backend app repository and run the following command.

```shell
npm run build
```

This will compile the backend source code and generate the `dist` folder containing the compiled .js files.


### 6. SSH into the VM and start the instances.

Go back to the terminal on the root directory of the main parent repository `buzzstreet-app`.

Run the following command to SSH into the VM:

```shell
vagrant ssh
```

Once in the VM, run the following commands:

```shell
sudo su -
cd /vagrant
```

This will allow you to assume the admin role and have root access.

The `/vagrant` is where vagrant will sync the project files into the VM.

Use the command:
```shell
ls -l
```

You can verify the files (you should be able to see the docker-compose.yml file in the directory).

In the `/vagrant` directory, run the command:

```shell
docker-compose up -d --build
```

This will pull/build the images configured in the docker-compose and Dockerfile(s) and create the containers accordingly.

Use the `--build` option if you have any changes made to the scripts, otherwise you have just run the command without the flag. This will allow the operation to be much faster as docker-compose does not have to build the images again.

```shell
docker-compose up -d
```

To verify the containers are running, you can use:

```shell
docker ps -a
```

Alternatively you can append the `watch` command to allow auto-refresh of the container statuses:

```shell
watch docker ps -a
```

Once verified you should be able to start the development.


## Connecting to the local VM's PostgreSQL and Redis.

A wide variety of software tools are available for connecting to and updating these resources.
- PostgreSQL: PgAdmin, IntelliJ IDEA Database
- Redis: redis-cli, IntelliJ IDEA Database

To find the connection details:
1. Since these instances are hosted inside the vagrant managed VM, the host IP will be the IP set inside the Vagrantfile, under the `config.vm.network "private_network"` configuration.
2. For PostgreSQL database:
   - port number: the vagrantfile `config.vm.network "forwarded_port"`, and the docker-compose.yml `service.postgres.ports` configuration.
   - login credential: docker-compose.yml `service.postgres.environment` configuration.
3. For Redis:
   - port number: the vagrantfile `config.vm.network "forwarded_port"`, and the docker-compose.yml `service.postgres.ports` configuration.

