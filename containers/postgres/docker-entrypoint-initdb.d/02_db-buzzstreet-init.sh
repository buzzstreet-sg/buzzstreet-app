#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username $POSTGRES_USER --dbname $POSTGRES_DB <<-EOSQL
  -- Acts as DBA for creating objects
  SET ROLE role_admin;

  CREATE ROLE role_webapp WITH LOGIN NOSUPERUSER NOINHERIT NOREPLICATION PASSWORD 'password';
  GRANT role_webapp TO role_admin;

  CREATE DATABASE db_buzzstreet WITH OWNER = role_admin ENCODING = 'UTF8' LC_COLLATE = 'en-US' LC_CTYPE = 'en-US'
      TEMPLATE=template0;
  COMMENT ON DATABASE db_buzzstreet IS 'Database for BuzzStreet application';
  REVOKE ALL PRIVILEGES ON DATABASE db_buzzstreet FROM PUBLIC;
  GRANT CONNECT, TEMPORARY ON DATABASE db_buzzstreet TO role_dev, role_webapp;

  -- Need to set role again after switching or connect to another database
  \connect db_buzzstreet;
  SET ROLE role_admin;

  CREATE SCHEMA schema_buzzstreet AUTHORIZATION role_admin;
  COMMENT ON SCHEMA schema_buzzstreet IS 'Default Schema for BuzzStreet application.';

  GRANT USAGE ON SCHEMA schema_buzzstreet to role_dev, role_webapp;

  -- To grant privileges to tables that will be created in the future
  ALTER DEFAULT PRIVILEGES IN SCHEMA schema_buzzstreet
      GRANT SELECT ON TABLES TO role_dev;
  ALTER DEFAULT PRIVILEGES IN SCHEMA schema_buzzstreet
      GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO role_webapp;

  -- To grant privileges to sequences that will be created in the future
  ALTER DEFAULT PRIVILEGES IN SCHEMA schema_buzzstreet
      GRANT SELECT ON SEQUENCES TO role_dev;
  ALTER DEFAULT PRIVILEGES IN SCHEMA schema_buzzstreet
      GRANT USAGE, SELECT, UPDATE ON SEQUENCES TO role_webapp;

  -- To grant privileges to routines, functions and procedures that will be created in the future
  ALTER DEFAULT PRIVILEGES IN SCHEMA schema_buzzstreet
      GRANT EXECUTE ON ROUTINES TO role_webapp;

  -- Additional privileges
  -- To grant privileges to types that will be created in the future
  ALTER DEFAULT PRIVILEGES IN SCHEMA schema_buzzstreet
      GRANT USAGE ON TYPES TO role_dev, role_webapp;
EOSQL

psql -v ON_ERROR_STOP=1 --username $POSTGRES_USER --dbname db_buzzstreet <<-EOSQL
  DROP SCHEMA public CASCADE;
EOSQL
