#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username $POSTGRES_USER --dbname $POSTGRES_DB <<-EOSQL
  CREATE ROLE role_admin WITH NOSUPERUSER NOINHERIT NOREPLICATION CREATEDB CREATEROLE;
  CREATE ROLE role_dev WITH NOSUPERUSER NOINHERIT NOREPLICATION;

  -- GRANT TO $POSTGRES_USER so we can create the database with them as owner with "SET ROLE <role>" statement
  GRANT role_admin to $POSTGRES_USER;
  GRANT role_dev to $POSTGRES_USER;

EOSQL
