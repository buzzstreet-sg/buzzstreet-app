# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "gbailey/amzn2"
  config.vm.box_check_update = true

  config.vm.host_name = "local"
  config.vm.network "private_network", ip: "172.31.22.155"

  # config.vm.synced_folder ".", "/vagrant", disabled: true

  if Vagrant::Util::Platform.windows?
      config.vbguest.auto_reboot = true
      config.vbguest.auto_update = true
  end

  config.vm.provider "virtualbox" do |vb|
     vb.gui = false
     vb.memory = "4096"
     # vb.memory = "6144"
  end

  # Allow host to interact the docker running in guest machine
  config.vm.network "forwarded_port", guest: 2375, host: 2375

  # Middleware
  config.vm.network "forwarded_port", guest: 5432, host: 5432
  config.vm.network "forwarded_port", guest: 6379, host: 6379

  # Shell Provisioner
  # Full list can be found here: https://www.vagrantup.com/docs/provisioning/shell
  $script = <<-SCRIPT
  echo "Start provisioning..."

  echo "sslverify=false" >> /etc/yum.conf
  yum install -y docker
  systemctl enable docker
  mkdir -p /etc/systemd/system/docker.service.d
  echo \"[Service] \nExecStart= \nExecStart=/usr/bin/dockerd -H unix:// -H tcp://0.0.0.0:2375\" >> /etc/systemd/system/docker.service.d/override.conf
  systemctl daemon-reload
  systemctl start docker

  curl -L -k "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose
  ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
  echo "127.0.0.1 host.docker.internal" >> /etc/hosts

  echo "Provisioning completed."
  SCRIPT

  config.vm.provision "shell", inline: $script

end
